package;

import haxe.io.Input;
import haxe.io.Eof;


class Main {
    
    static function main() {
        
        var input : Input = Sys.stdin();
        var LINE : String;
        var LINE_NUMBER : Int = 1;
        try {
            while( (LINE = input.readLine()).length > 0 ) {
                trace("line:", LINE);
                LINE_NUMBER++;
            }
        } catch( e : Eof ) {
            
        }
        
    }
    
}